#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from app import app


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()


if __name__ == '__main__':
    unittest.main()
