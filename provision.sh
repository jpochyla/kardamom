#!/bin/bash

# Stop on first error
set -e

# Switch to unicode locale
export LC_ALL="en_US.UTF-8"
update-locale LC_ALL=en_US.UTF_8

# Install packages
apt-get update
apt-get install -qy postgresql postgresql-server-dev-all
apt-get install -qy python-dev python-pip python-virtualenv

# Create PostgreSQL user and database
sudo -u postgres createuser -s vagrant
sudo -u vagrant createdb kardamom

# Create virtualenv and load it after login
sudo -u vagrant virtualenv /home/vagrant/venv
echo "source /home/vagrant/venv/bin/activate" >> /home/vagrant/.profile
echo "cd /vagrant" >> /home/vagrant/.profile

echo -e "\n"
echo 'export DEBUG="True"' >> /home/vagrant/.profile
echo 'export SECRET_KEY="ttXWM2fOqAaFZd09wMbO"' >> /home/vagrant/.profile
echo 'export ADMIN_USERNAME="admin"' >> /home/vagrant/.profile
echo 'export ADMIN_PASSWORD="nimda"' >> /home/vagrant/.profile
echo 'export DATABASE_URL="postgresql+psycopg2:///kardamom"' >> /home/vagrant/.profile
