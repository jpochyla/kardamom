# -*- coding: utf-8 -*-

from os import environ
from flask import Flask, render_template, request, redirect, url_for
from models import Product, Cuisine, Shop
from database import db
from admin import admin

app = Flask(__name__)

app.config['DEBUG'] = app.config['SQLALCHEMY_ECHO'] = environ.get('DEBUG', False)

app.config['SECRET_KEY'] = environ['SECRET_KEY']
app.config['ADMIN_USERNAME'] = environ['ADMIN_USERNAME']
app.config['ADMIN_PASSWORD'] = environ['ADMIN_PASSWORD']
app.config['SQLALCHEMY_DATABASE_URI'] = environ['DATABASE_URL']

db.init_app(app)
admin.init_app(app)


@app.route('/')
def index():
    cuisines = Cuisine.query.filter(Cuisine.shops.any()).all()

    return render_template('index.html', cuisines=cuisines)


@app.route('/product')
def search():
    query = request.args.get('query')
    if not query:
        return redirect(url_for('index'))

    products = Product.query.filter_by_fulltext(query).all()
    if len(products) == 1:
        return redirect(url_for('product', query=query, product_id=products[0].id))

    cuisines = Cuisine.query.filter_by_products(products).all()
    product_shops = Shop.query.filter_by_products(products).all()
    cuisine_shops = Shop.query.filter_by_cuisines(cuisines) \
                              .exclude(product_shops) \
                              .all()

    return render_template('shops_search.html',
                           query=query,
                           products=products,
                           cuisines=cuisines,
                           product_shops=product_shops,
                           cuisine_shops=cuisine_shops)


@app.route('/product/<product_id>')
def product(product_id):
    query = request.args.get('query')
    product = Product.query.get_or_404(product_id)
    cuisines = Cuisine.query.filter_by_products([product]).all()
    product_shops = Shop.query.filter_by_products([product]).all()
    cuisine_shops = Shop.query.filter_by_cuisines(cuisines) \
                              .exclude(product_shops) \
                              .all()

    return render_template('shops_by_product.html',
                           query=query,
                           product=product,
                           cuisines=cuisines,
                           product_shops=product_shops,
                           cuisine_shops=cuisine_shops)


@app.route('/cuisine/<cuisine_id>')
def cuisine(cuisine_id):
    cuisine = Cuisine.query.get_or_404(cuisine_id)
    shops = Shop.query.filter_by_cuisines([cuisine]).all()

    return render_template('shops_by_cuisine.html', cuisine=cuisine, shops=shops)


@app.route('/shop/<shop_id>')
def shop(shop_id):
    shop = Shop.query.get_or_404(shop_id)

    return render_template('shop.html', shop=shop)
