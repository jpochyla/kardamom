# -*- coding: utf-8 -*-

from functools import wraps
from flask import current_app, request, Response
from flask.ext.admin import Admin, AdminIndexView, expose
from flask.ext.admin.contrib.sqlamodel import ModelView
from models import Product, Cuisine, Shop
from database import db


def check_credentials(username, password):
    admin_username = current_app.config['ADMIN_USERNAME']
    admin_password = current_app.config['ADMIN_PASSWORD']
    return username == admin_username and password == admin_password


def is_authenticated():
    auth = request.authorization
    return auth and check_credentials(auth.username, auth.password)


def authenticate():
    headers = {'WWW-Authenticate': 'Basic realm="Login Required"'}
    return Response('Please log in first.', 401, headers)


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_authenticated():
            return authenticate()
        return f(*args, **kwargs)
    return decorated


class RequiresAuthMixin(object):
    def is_accessible(self):
        return is_authenticated()


class IndexView(AdminIndexView):
    @expose('/')
    @requires_auth
    def index(self):
        return self.render('admin/index.html')


class AuthModelView(ModelView, RequiresAuthMixin):
    pass


admin = Admin(index_view=IndexView(name=u'Home'), name=u'Kardamom')
admin.add_view(AuthModelView(Product, db.session, name=u'Produkty'))
admin.add_view(AuthModelView(Cuisine, db.session, name=u'Kuchyně'))
admin.add_view(AuthModelView(Shop, db.session, name=u'Obchody'))
