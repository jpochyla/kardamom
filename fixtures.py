# -*- coding: utf-8 -*-

cuisine_names = [u'Americká', u'Indie', u'Thajsko', u'Filipíny', u'Čína',
                 u'Japonsko', u'Řecko', u'Vietnam', u'Itálie', u'Francie',
                 u'Pákistán', u'Rusko', u'Slovinsko', u'Bulharsko']

product_names = [u'Anýz celý', u'Anýz mletý', u'Asa foetida',
                 u'Badyán celý', u'Badyán mletý', u'Bazalka',
                 u'Bobkový list celý', u'Bobkový list mletý', u'Chilli drcené',
                 u'Chilli mleté', u'Chilli papričky malé', u'Chilli papričky velké',
                 u'Chilli pikant', u'Cibule granulovaná', u'Cibule plátky',
                 u'Cibule prášek', u'Citronová kůra drcená', u'Citronová kůra mletá',
                 u'Cumin celý', u'Cumin mletý', u'Černucha',
                 u'Česnek granule malé', u'Česnek granule velké', u'Česnek medvědí',
                 u'Česnek plátky', u'Česnek prášek', u'Estragon',
                 u'Fenykl', u'Glutaman', u'Hořčice hnědá',
                 u'Hořčice žlutá celá', u'Horčice žlutá mletá', u'Hřebíček celý',
                 u'Hřebíček mletý', u'Jalovec celý', u'Jalovec mletý',
                 u'Kardamom mletý', u'Kardamom semínka', u'Kardamom tobolky',
                 u'Kmín celý', u'Kmín drcený', u'Kmín mletý',
                 u'Kopr', u'Koriandr celý', u'Koriandr lístky',
                 u'Koriandr mletý', u'Kurkuma', u'Lékořice',
                 u'Libeček', u'Majoránka', u'Máta peprná',
                 u'Muškátový květ celý', u'Muškátový květ mletý', u'Muškátový ořech celý',
                 u'Muškátový ořech mletý', u'Nové koření celé', u'Nové koření mleté',
                 u'Oregano', u'Paprika granule červené', u'Paprika granule zelené',
                 u'Paprika pálivá', u'Paprika pálivá vločky', u'Paprika sladká maďarská',
                 u'Paprika sladká španělská', u'Paprika uzená sladká', u'Pažitka',
                 u'Pepř barevný celý', u'Pepř bílý celý', u'Pepř bílý drcený',
                 u'Pepř bílý mletý', u'Pepř cayenský', u'Pepř černý celý',
                 u'Pepř černý drcený', u'Pepř černý mletý', u'Pepř dlouhý',
                 u'Pepř růžový', u'Pepř sečuánský celý', u'Pepř zelený celý',
                 u'Pepř zelený drcený', u'Petržel', u'Pískavice celá',
                 u'Pískavice mletá', u'Pomerančová kůra drcená', u'Pomerančová kůra mletá',
                 u'Puškvorec', u'Rozmarýn', u'Saturejka',
                 u'Sezamové semínko', u'Skořice celá 1,5 cm', u'Skořice celá 8 cm',
                 u'Skořice drcená', u'Skořice mletá', u'Sůl himálajská',
                 u'Sůl hrubozrnná', u'Sumah', u'Sušená rajčata',
                 u'Šafrán celý', u'Šafrán mletý', u'Šalvěj',
                 u'Šípek', u'Tymián', u'Vanilka (1 lusk)',
                 u'Vanilka mletá', u'Zázvor celý', u'Zázvor drcený',
                 u'Zázvor mletý']
