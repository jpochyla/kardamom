# -*- coding: utf-8 -*-

from sqlalchemy.dialects.postgresql import ARRAY
from database import db
import fixtures
import itertools


def create_test_models():
    cuisines = [Cuisine(name=c) for c in fixtures.cuisine_names]
    products = [Product(name=p) for p in fixtures.product_names]
    for i, p in enumerate(products):
        p.cuisines.append(cuisines[i % len(cuisines)])
    for obj in itertools.chain(cuisines, products):
        db.session.add(obj)
    db.session.commit()


shop_with_product = db.Table(
    'shop_with_product', db.metadata,
    db.Column('shop_id', db.Integer, db.ForeignKey('shop.id'), primary_key=True),
    db.Column('product_id', db.Integer, db.ForeignKey('product.id'), primary_key=True))

shop_with_cuisine = db.Table(
    'shop_with_cuisine', db.metadata,
    db.Column('shop_id', db.Integer, db.ForeignKey('shop.id'), primary_key=True),
    db.Column('cuisine_id', db.Integer, db.ForeignKey('cuisine.id'), primary_key=True))

product_with_cuisine = db.Table(
    'product_with_cuisine', db.metadata,
    db.Column('product_id', db.Integer, db.ForeignKey('product.id'), primary_key=True),
    db.Column('cuisine_id', db.Integer, db.ForeignKey('cuisine.id'), primary_key=True))


class ShopQuery(db.Query):
    def filter_by_cuisines(self, cuisines):
        match_cuisine = Cuisine.id.in_([c.id for c in cuisines])
        return self.join(Shop.cuisines).filter(match_cuisine)

    def filter_by_products(self, products):
        match_product = Product.id.in_([p.id for p in products])
        return self.join(Shop.products).filter(match_product)

    def exclude(self, shops):
        return self.filter(~Shop.id.in_([s.id for s in shops]))


class CuisineQuery(db.Query):
    def filter_by_products(self, products):
        match_product = Product.id.in_([p.id for p in products])
        return self.join(Cuisine.products).filter(match_product)


class ProductQuery(db.Query):
    def filter_by_fulltext(self, query):
        match_aliases = Product.aliases.op('@>')([query])
        match_tags = Product.tags.op('@>')([query])
        match_name = Product.name == query
        return self.filter(match_name | match_aliases | match_tags)


class Cuisine(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.UnicodeText)

    query_class = CuisineQuery

    def __unicode__(self):
        return self.name


class Shop(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.UnicodeText)
    tin = db.Column(db.UnicodeText)
    vat = db.Column(db.UnicodeText)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    city = db.Column(db.UnicodeText)
    street = db.Column(db.UnicodeText)
    zipcode = db.Column(db.UnicodeText)
    website = db.Column(db.UnicodeText)
    email = db.Column(db.UnicodeText)
    phone = db.Column(db.UnicodeText)

    cuisines = db.relationship('Cuisine', backref='shops',
                               secondary=shop_with_cuisine)
    products = db.relationship('Product', backref='shops',
                               secondary=shop_with_product)

    query_class = ShopQuery

    def __unicode__(self):
        return self.name


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.UnicodeText, index=True)
    tags = db.Column(ARRAY(db.UnicodeText), index=True)
    aliases = db.Column(ARRAY(db.UnicodeText), index=True)
    description = db.Column(db.UnicodeText)

    cuisines = db.relationship('Cuisine', backref='products',
                               secondary=product_with_cuisine)

    query_class = ProductQuery

    def __unicode__(self):
        return self.name
