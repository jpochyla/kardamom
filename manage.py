#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.script import Server, Manager
from app import app
from database import db

manager = Manager(app)
manager.add_command("runserver", Server(host="0.0.0.0"))


@manager.command
def drop_db():
    "Drops database tables"
    if prompt_bool("Are you sure you want to lose all your data"):
        db.drop_all()

@manager.command
def create_db():
    "Creates database tables from sqlalchemy models"
    db.create_all()

@manager.command
def populate_db():
    "Populate database with default data"
    from models import create_test_models
    create_test_models()


if __name__ == "__main__":
    manager.run()
